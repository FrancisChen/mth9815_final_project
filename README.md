# README #
### MTH9815 Software Engineering for Finance ###
### Bond Trading System Final Project ###
### Author @FrancisChen ###

# NOTE #
### 1. Most of the project are done independently by myself ###

### 2. Zilun Shen helped me a lot in implementing the following files: InputFileGenerator.hpp/.cpp, BondHistoricalDataService.hpp/.cpp and main.cpp. But I code every line by myself. Credit to him. ### 

# HOW TO RUN #
## 1. Requirement: ##
### Boost Library ###
### CMake3.5.2 ###
### g++6.2.0 ###

## 2. To run in Terminal/Command, input and press enter: ##
### cmake . ###
### make ###
### ./main.out ###

## 3. If the boost path is not found, please modify the CMakeLists.txt, below g++ config: ##
### SET(CMAKE_CXX_FLAGS "-std=c++11 -O3 -I /yourpath/boost") ###