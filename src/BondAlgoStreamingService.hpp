//
//  BondAlgoStreamingService.h
//  Bond_Trading_System
//
//  Created by Huiyou Chen on 12/5/16.
//  Copyright © 2016 Francis Chen. All rights reserved.
//

#ifndef BondAlgoStreamingService_hpp
#define BondAlgoStreamingService_hpp
#include "SOA.hpp"
#include "Products.hpp"
#include "StreamingService.hpp"
#include "BondPricingService.hpp"

class AlgoStream : public PriceStream<Bond>
{
public:
    AlgoStream() = default;
    virtual ~AlgoStream() = default;
    AlgoStream(Bond& bond, PriceStreamOrder& bid, PriceStreamOrder& offer);
    AlgoStream(const PriceStream<Bond>& source);
    const PriceStream<Bond>& GetAlgoStream() const;
private:
    const PriceStream<Bond>* _priceStream;
};

class AlgoStreamingService : public Service<string, AlgoStream>
{
public:
    virtual void PublishPrice(const PriceStream<Bond>& stream) = 0;
};

class BondAlgoStreamingService : public AlgoStreamingService
{
public:
    BondAlgoStreamingService() = default;

    virtual ~BondAlgoStreamingService() = default;

    AlgoStream& GetData(string key) override;

    void OnMessage(AlgoStream& data) override;

    void AddListener(ServiceListener<AlgoStream> *listener) override;

    const vector<ServiceListener<AlgoStream>*>& GetListeners() const override;

    void PublishPrice(const PriceStream<Bond>& stream) override;

private:
    map<string, AlgoStream> _algoStream;
    vector<ServiceListener<AlgoStream>*> _algoStreamListeners;
};

class BondAlgoStreamingListener : public ServiceListener<Price<Bond> >
{
public:
    BondAlgoStreamingListener(BondPricingService &prcsrv,
                              BondAlgoStreamingService &algostrsrv);
    void ProcessAdd(Price<Bond> &data) override;
    void ProcessRemove(Price<Bond> &data) override {};
    void ProcessUpdate(Price<Bond> &data) override {};
private:
    BondAlgoStreamingService *algostrsrv_ptr;
};

#endif /* BondAlgoStreamingService_h */
