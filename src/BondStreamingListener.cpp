//
//  BondStreamingListener.cpp
//  Bond_Trading_System
//
//  Created by Huiyou Chen on 12/18/16.
//  Copyright © 2016 Francis Chen. All rights reserved.
//

#include "BondStreamingListener.hpp"
BondStreamingListener::BondStreamingListener(BondAlgoStreamingService &algoService, BondStreamingService & streamService)
{
    algoService.AddListener(this);
    _bondStreamService = & streamService;
}


void BondStreamingListener::ProcessAdd(AlgoStream& data)
{
    _bondStreamService->PublishPrice(data.GetAlgoStream());
}
