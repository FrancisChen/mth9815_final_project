/**
 * riskservice.hpp
 * Defines the data types and Service for fixed income risk.
 *
 * @author Breman Thuraisingham
 * Implemented by Huiyou Chen on 12/5/16.
 * Copyright © 2016 Francis Chen. All rights reserved.
 */
#ifndef RISK_SERVICE_HPP
#define RISK_SERVICE_HPP

#include "SOA.hpp"
#include "PositionService.hpp"
#include "Products.hpp"

/**
 * PV01 risk.
 * Type T is the product type.
 */
template<typename T>
class PV01
{

public:

    PV01();
    // ctor for a PV01 value
    PV01(const T& product, double pv01, long quantity);

    // Get the product on this PV01 value
    const T& GetProduct() const;

    // Get the PV01 value
    double GetPV01() const;

    // Get the quantity that this risk value is associated with
    long GetQuantity() const;

    void UpdateQuantity(Position<T> &position);

private:
    T _product;
    double _pv01;
    long _quantity;
};

/**
 * A bucket sector to bucket a group of securities.
 * We can then aggregate bucketed risk to this bucket.
 * Type T is the product type.
 */
template<typename T>
class BucketedSector
{

public:

    // ctor for a bucket sector
    BucketedSector(const vector<T> &_products, string _name);

    // Get the products associated with this bucket
    const vector<T>& GetProducts() const;

    // Get the name of the bucket
    const string& GetName() const;

private:
    vector<T> products;
    string name;

};

/**
 * Risk Service to vend out risk for a particular security and across a risk bucketed sector.
 * Keyed on product identifier.
 * Type T is the product type.
 */
template<typename T>
class RiskService : public Service<string, PV01<T> >{

public:

    // Add a position that the service will risk
    virtual void AddPosition(Position<T> &position) = 0;
    // Get the bucketed risk for the bucket sector
    virtual const double GetBucketedRisk(const BucketedSector<T> &sector) const = 0;
    
};

template<typename T>
PV01<T>::PV01(): _product(Bond()), _pv01(0.0), _quantity(0){

}
template<typename T>
PV01<T>::PV01(const T& product, double pv01, long quantity): _product(product), _pv01(pv01), _quantity(quantity)
{
}

template<typename T>
const T& PV01<T>::GetProduct() const
{
    return _product;
}

template<typename T>
double PV01<T>::GetPV01() const
{
    return _pv01;
}

template<typename T>
long PV01<T>::GetQuantity() const
{
    return _quantity;
}

template<typename T>
void PV01<T>::UpdateQuantity(Position<T>& position){
    _quantity += position.GetAggregatePosition();
}

template<typename T>
BucketedSector<T>::BucketedSector(const vector<T>& _products, string _name) :
products(_products)
{
    name = _name;
}

template<typename T>
const vector<T>& BucketedSector<T>::GetProducts() const
{
    return products;
}

template<typename T>
const string& BucketedSector<T>::GetName() const
{
    return name;
}



#endif
