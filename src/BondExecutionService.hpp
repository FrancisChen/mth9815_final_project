//
//  BondExecutionService.h
//  Bond_Trading_System
//
//  Created by Huiyou Chen on 12/5/16.
//  Copyright © 2016 Francis Chen. All rights reserved.
//

#ifndef BondExecutionService_hpp
#define BondExecutionService_hpp
#include "SOA.hpp"
#include "Products.hpp"
#include "ExecutionService.hpp"

class BondExecutionService: public ExecutionService<Bond>
{
public:
    BondExecutionService() = default;

    virtual ~BondExecutionService() = default;

    Bond MakeBond(string& prodcutId);

    ExecutionOrder<Bond>& GetData(string key) override;

    void OnMessage(ExecutionOrder<Bond>& data) override;

    void AddListener(ServiceListener<ExecutionOrder<Bond> > *listener) override;

    const vector<ServiceListener<ExecutionOrder<Bond> >*>& GetListeners() const override;

    // Execute an order on a market
    void ExecuteOrder(ExecutionOrder<Bond>& order, Market market) override;

    friend ostream& operator<<(ostream &output, const BondExecutionService &source);
    

private:
    map<string, ExecutionOrder<Bond> > _execution;
    vector<ServiceListener<ExecutionOrder<Bond> >*> _executionListeners;
};


#endif /* BondExecutionService_h */
