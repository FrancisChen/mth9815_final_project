//
//  BondHistoricalDataService.cpp
//  Bond_Trading_System
//
//  Created by Huiyou Chen on 12/19/16.
//  Copyright © 2016 Francis Chen. All rights reserved.
//

#include "BondHistoricalDataService.hpp"
BondHistoricalDataService::BondHistoricalDataService(){
    all_output.insert({SERVICE_KEY_POSITION,
        std::make_shared<FileOutput>(HISTORYDATA_OUTPUT_POSITION)});
    all_output.insert({SERVICE_KEY_RISK,
        std::make_shared<FileOutput>(HISTORYDATA_OUTPUT_RISK)});
    all_output.insert({SERVICE_KEY_EXECUTION,
        std::make_shared<FileOutput>(HISTORYDATA_OUTPUT_EXECUTION)});
    all_output.insert({SERVICE_KEY_STREAMING,
        std::make_shared<FileOutput>(HISTORYDATA_OUTPUT_STREAMING)});
    all_output.insert({SERVICE_KEY_INQUIRY,
        std::make_shared<FileOutput>(HISTORYDATA_OUTPUT_INQUIRY)});
}
void BondHistoricalDataService::PersistData(string persistKey, const HistoricalData<Bond>& data)
{
    auto writer_ptr = all_output.find(persistKey);
    if(writer_ptr != all_output.end()){
        writer_ptr->second->OutputLine(HistoricalData<Bond>(data).GetHistoricalData());
    }
}

HistoricalData<Bond>& BondHistoricalDataService::GetData(string key){
    if(_histData.find(key)!=_histData.end())
        return _histData[key];
    else {
        cout << "No Historical Data!" << endl;
        exit(-1);
    }
}

void BondHistoricalDataService::OnMessage(HistoricalData<Bond>& data){

}

void BondHistoricalDataService::AddListener(ServiceListener<HistoricalData<Bond> > *listener){
    _histDataListeners.push_back(listener);
}

const vector<ServiceListener<HistoricalData<Bond> >*>& BondHistoricalDataService::GetListeners() const{
    return _histDataListeners;
}
