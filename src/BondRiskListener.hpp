//
//  BondRiskListener.hpp
//  Bond_Trading_System
//
//  Created by Huiyou Chen on 12/18/16.
//  Copyright © 2016 Francis Chen. All rights reserved.
//

#ifndef BondRiskListener_hpp
#define BondRiskListener_hpp
#include "BondPositionService.hpp"
#include "BondRiskService.hpp"

class BondRiskListener : public ServiceListener<Position<Bond> >
{
public:
    BondRiskListener() = default;
    BondRiskListener(BondPositionService& positionService,
                         BondRiskService& riskService);
    virtual ~BondRiskListener() = default;
    void ProcessAdd(Position<Bond> &data) override;
    void ProcessRemove(Position<Bond> &data) override {};
    void ProcessUpdate(Position<Bond> &data) override {};
    void AddReceiver(BondRiskService& receiver);

private:
    BondRiskService* _riskService;
};
#endif /* BondRiskListener_hpp */
