//
//  BondExecutionListener.hpp
//  Bond_Trading_System
//
//  Created by Huiyou Chen on 12/18/16.
//  Copyright © 2016 Francis Chen. All rights reserved.
//

#ifndef BondExecutionListener_hpp
#define BondExecutionListener_hpp
#include "BondExecutionService.hpp"
#include "BondAlgoExecutionService.hpp"

class BondExecutionListener : public ServiceListener<AlgoExecution>
{
public:
    BondExecutionListener(BondAlgoExecutionService & algoService,
                          BondExecutionService & executionService);
    void ProcessAdd(AlgoExecution &data) override;
    void ProcessRemove(AlgoExecution &data) override {};
    void ProcessUpdate(AlgoExecution &data) override {};
private:
    BondExecutionService * _bondExecutionService;
};

#endif /* BondExecutionListener_hpp */
