//
//  BondStreamingConnector.hpp
//  Bond_Trading_System
//
//  Created by Huiyou Chen on 12/18/16.
//  Copyright © 2016 Francis Chen. All rights reserved.
//

#ifndef BondStreamingConnector_hpp
#define BondStreamingConnector_hpp
#include "SOA.hpp"
#include "BondStreamingService.hpp"
#include "Products.hpp"
#include "DataTranslator.hpp"
#include <fstream>
#include <sstream>

class BondStreamingConnector: public Connector<PriceStream<Bond> >, private FileOutput
{

public:
    BondStreamingConnector() = default;
    ~BondStreamingConnector() = default;
    BondStreamingConnector(string filepath): FileOutput(filepath){}
    // override connector FileReader and Publish
    void Publish(const PriceStream<Bond> & data) override{
            FileOutput::OutputLine({data.GetProduct().GetProductId(),
                to_string(data.GetBidOrder().GetPrice()),
                to_string(data.GetBidOrder().GetHiddenQuantity()),
                to_string(data.GetBidOrder().GetVisibleQuantity()),
                to_string(data.GetOfferOrder().GetPrice()),
                to_string(data.GetOfferOrder().GetHiddenQuantity()),
                to_string(data.GetOfferOrder().GetVisibleQuantity())
            });
    }
    void FileReader(string& filename) override{}
};

//BondStreamingConnector::BondStreamingConnector(string filepath){
//    ofstream output(filepath);
//    output << "Product ID, Bid Price, Visible Quantity, Hidden Quantity, Offer Price, Visible Quantity, Hidden Quantity\n"
//    output<< *this;
//}

#endif /* BondStreamingConnector_hpp */
