//
//  BondPositionListener.hpp
//  Bond_Trading_System
//
//  Created by Huiyou Chen on 12/18/16.
//  Copyright © 2016 Francis Chen. All rights reserved.
//

#ifndef BondPositionListener_hpp
#define BondPositionListener_hpp
#include "BondPositionService.hpp"
#include "BondTradeBookingService.hpp"

class BondPositionListener : public ServiceListener<Trade<Bond> >
{
public:
    BondPositionListener() = default;
    BondPositionListener(BondTradeBookingService& tradeService,
                         BondPositionService& positionService);
    virtual ~BondPositionListener() = default;
    void ProcessAdd(Trade<Bond> &data) override;
    void ProcessRemove(Trade<Bond> &data) override {};
    void ProcessUpdate(Trade<Bond> &data) override {};
    void AddReceiver(BondPositionService& receiver);
private:
    BondPositionService* _positionService;
};
#endif /* BondPositionListener_hpp */
