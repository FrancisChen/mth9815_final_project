//
//  InputFileGenerator.hpp
//  Bond_Trading_System
//
//  Created by Huiyou Chen on 12/18/16.
//  Copyright © 2016 Francis Chen. All rights reserved.
//

#ifndef InputFileGenerator_hpp
#define InputFileGenerator_hpp

#include "DataTranslator.hpp"

// Generate trade input file
void GetTradeFile(string filename);

// Generate price input file
void GetPriceFile(string filename);

// Generate marketdata input file
void GetMarketDataFile(string filename);

// Generate inquiry input file
void GetInquiryFile(string filename);

// Total number of bonds
const int NUM_OF_BONDS = 6;
const std::string BOND_YEAR_2  = "912828A75";
const std::string BOND_YEAR_3  = "912828G95";
const std::string BOND_YEAR_5  = "912828G87";
const std::string BOND_YEAR_7  = "912828U57";
const std::string BOND_YEAR_10 = "912828U24";
const std::string BOND_YEAR_30 = "912810RU4";

const std::vector<std::string> CUSIPS_LIST = {
    BOND_YEAR_2 ,
    BOND_YEAR_3 ,
    BOND_YEAR_5 ,
    BOND_YEAR_7 ,
    BOND_YEAR_10,
    BOND_YEAR_30
};

const std::vector<double> PV01_LIST = {
    0.01974732,
    0.02333471,
    0.01587385,
    0.02387587,
    0.07124954,
    0.05928573
};

const std::vector<BondIdType> BOND_ID_TYPE_LIST{CUSIP, CUSIP, CUSIP,  CUSIP,  CUSIP,  CUSIP};
const std::vector<string> TICKER_LIST{"T", "T", "T", "T", "T", "T"};
const std::vector<float> COUPON_LIST{0.05f, 0.05f, 0.05f, 0.05f, 0.05f, 0.05f};
const std::vector<date> MATURITY_LIST{
    date(2016, Nov, 25),
    date(2016, Nov, 25),
    date(2016, Nov, 25),
    date(2016, Nov, 25),
    date(2016, Nov, 25),
    date(2016, Nov, 25)
};

const std::vector<std::string> POSITION_BOOK_LIST{
    "TRSY1",
    "TRSY2",
    "TRSY3"
};

/*********************************************************************
 *
 *   Describe the input file for each service
 *
 ********************************************************************/


/*********************************************************************
 *
 *	 This part is for HistoricalDataService
 *   Describe the files and keys for each service
 *
 ********************************************************************/

//const std::string SERVICE_KEY_POSITION    = "Position Service";
//const std::string SERVICE_KEY_RISK        = "Risk Service";
//const std::string SERVICE_KEY_EXECUTION   = "Execution Service";
//const std::string SERVICE_KEY_STREAMING   = "Streaming Service";
//const std::string SERVICE_KEY_INQUIRY	 = "Inquiry Service";
//
//const std::vector<std::string> SERVICE_KEY_LIST{
//    SERVICE_KEY_POSITION,
//    SERVICE_KEY_RISK,
//    SERVICE_KEY_EXECUTION,
//    SERVICE_KEY_STREAMING,
//    SERVICE_KEY_INQUIRY
//};
//
//const std::string HISTORYDATA_OUTPUT_POSITION = "./output/position.txt";
//const std::string HISTORYDATA_OUTPUT_RISK = "./output/risk.txt";
//const std::string HISTORYDATA_OUTPUT_EXECUTION = "./output/execution.txt";
//const std::string HISTORYDATA_OUTPUT_STREAMING = "./output/streaming.txt";
//const std::string HISTORYDATA_OUTPUT_INQUIRY = "./output/allinquiries.txt";

#endif /* InputFileGenerator_hpp */
