//
//  BondTradeBookingService.cpp
//  Bond_Trading_System
//
//  Created by Huiyou Chen on 12/16/16.
//  Copyright © 2016 Francis Chen. All rights reserved.
//

#include "BondTradeBookingService.hpp"

BondTradeBookingService::~BondTradeBookingService(){

}

Trade<Bond>& BondTradeBookingService::GetData(string key){
    if(_tradingBook.find(key)!=_tradingBook.end()){
        cout << "No Trade Data" << endl;
        exit(-1);
    }
    else return _tradingBook[key];;
}

void BondTradeBookingService::OnMessage(Trade<Bond>& data){
    BookTrade(data);
    for(auto iter : _tradingListeners){
        iter->ProcessAdd(data);
    }

}

void BondTradeBookingService::AddListener(ServiceListener<Trade<Bond> > *listener)
{
    _tradingListeners.push_back(listener);
}

const vector<ServiceListener<Trade<Bond> >*>& BondTradeBookingService::GetListeners() const{
    return _tradingListeners;
}

void BondTradeBookingService::BookTrade(const Trade<Bond>& trade){
    _tradingBook[trade.GetTradeId()] = trade;
}

BondTradeBookingService& BondTradeBookingService::operator=(BondTradeBookingService& source)
{
    if(&source == this) return *this;
    for(auto iter : source._tradingBook)
        _tradingBook[iter.first] = iter.second;
    _tradingListeners = source._tradingListeners;
    return *this;
}

ostream& operator<<(ostream &output, const BondTradeBookingService &source){
    for(auto iter : source._tradingBook){
        output << iter.second.GetProduct() << iter.second.GetTradeId() << ", " << iter.second.GetBook() << ", " << iter.second.GetQuantity() << ", " << iter.second.GetSide() << "\n";
    }
    return output;
}
