//
//  DataTranslator.cpp
//  Bond_Trading_System
//
//  Created by Huiyou Chen on 12/19/16.
//  Copyright © 2016 Francis Chen. All rights reserved.
//

#include "DataTranslator.hpp"
date DateTranslator(string& str){
    date maturity;
    if(str=="912828A75") maturity = *new date(2018,Dec,31);
    if(str=="912828G95") maturity = *new date(2019,Dec,31);
    if(str=="912828G87") maturity = *new date(2021,Dec,31);
    if(str=="912828U57") maturity = *new date(2023,Nov,30);
    if(str=="912828U24") maturity = *new date(2026,Nov,15);
    if(str=="912810RU4") maturity = *new date(2046,Nov,15);
    else maturity = *new date(2018,Dec,31);
    return maturity;
}

double PriceTranslator(string& str){
    string s1 = str.substr(0,2);
    double num = stoi(s1);
    string s2 = str.substr(4,5);
    num += stoi(s2)/32.;
    if(str[6] == '+') num += 4./256.;
    else num += (str[6]-'0')/256.;
    return num;
}

string PriceToString(double dprice){
    string strprice;
    int big = static_cast<int>(dprice);
    dprice -= big;
    strprice = std::to_string(big);
    strprice += '-';
    // round to 1/256
    int iprice = static_cast<int>(dprice * 256 + 0.5);
    int r = iprice % 8;
    strprice += std::to_string(iprice / 80);
    strprice += std::to_string(iprice / 8 % 10);
    if(r == 4)	strprice += '+';
    else strprice += std::to_string(r);
    return strprice;
}

double SpreadTranslator(string& str){
    double tmp = str[0] - '0';
    string s5 = str.substr(2,3);
    tmp += stoi(s5)/32.;
    if(str[4] == '+') tmp += 4./256.;
    else tmp += (str[4]-'0')/256.;
    return tmp;

}

string BookTranslator(string& str){
    if(str=="TRSY1" || str == "0") return Book[0];
    if(str=="TRSY2" || str == "1") return Book[1];
    if(str=="TRSY3" || str == "2") return Book[2];
    return Book[0];
}

Side SideTranslator(string& str){
    Side side;
    if(str == "BUY" || str == "0") side = BUY;
    if(str == "SELL" || str == "0") side = SELL;
    else side = BUY;
    return side;
}

InquiryState StateTranslator(string& str){
    if(str =="0") return RECEIVED;
    if(str =="1") return QUOTED;
    if(str =="2") return DONE;
    if(str =="3") return REJECTED;
    else return CUSTOMER_REJECTED;
}

TradeData GetTradeData(string& ss){
    stringstream source(ss);
    TradeData trade_data;
    string str;
    //get ticker
    getline(source, str, ',');
    trade_data.ticker = str;

    // get the cusip number and transform to maturity
    getline(source, str, ',');
    trade_data.cusip = str;

    // get the book: TRSY1, TRSY2, TRSY3
    getline(source, str, ',');
    trade_data.book = BookTranslator(str);

    // get the quantity
    getline(source, str, ',');
    trade_data.quantity = stoi(str);

    // get the side: BUY or SELL
    getline(source, str, ',');
    trade_data.side = SideTranslator(str);

    return trade_data;
}

PriceData GetBondPrice(string &ss){
    stringstream source(ss);
    PriceData price;
    string str;
    // get the cusip number and transform to maturity
    getline(source, str, ',');
    price.cusip = str;

    // get the price
    getline(source, str, ',');
    price.mid = PriceTranslator(str);

    // get spread
    getline(source, str, ',');
    price.spread = SpreadTranslator(str);

    return price;
}


MarketData GetMarketData(string& ss){
    stringstream source(ss);
    MarketData market_data; map<double,long> mp;
    string str;
    double price; long quantity;
    getline(source, str, ',');
    market_data.cusip = str;

    for(int i=1; i<=5; ++i){
        getline(source, str, ',');
        price = PriceTranslator(str);
        getline(source, str, ',');
        quantity = stoi(str);
        Order bidorder(price, quantity, PricingSide::BID);
        market_data.bidStack.push_back(bidorder);
    }

    for(int i=1; i<=5; ++i){
        getline(source, str, ',');
        price = PriceTranslator(str);
        getline(source, str, ',');
        quantity = stoi(str);
        Order offerorder(price, quantity, PricingSide::OFFER);
        market_data.offerStack.push_back(offerorder);
    }
    return market_data;
}


InquiryData GetInquiryData(string &ss){
    stringstream source(ss);
    InquiryData inquiry;
    string str;
    getline(source, str, ',');
    inquiry.inquiryId = str;

    getline(source, str, ',');
    inquiry.cusip = str;

    getline(source, str, ',');
    if(str == "0") inquiry.side = BUY;
    else inquiry.side = SELL;

    getline(source, str, ',');
    inquiry.quantity = stoi(str);

    getline(source, str, ',');
    inquiry.price = PriceTranslator(str);

    getline(source, str, ',');
    inquiry.state = StateTranslator(str);
    
    return inquiry;
}

FileOutput::FileOutput(const string& filePath): _filePath(filePath)
{
    _output.open(filePath);
}

void FileOutput::OutputLine(const vector<string>& lines)
{
    copy(lines.begin(), lines.end(), ostream_iterator<string>(_output, ","));
    _output << endl;
}

FileOutput::~FileOutput()
{
    _output.close();
}
