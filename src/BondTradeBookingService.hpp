
//
//  BondTradeBookingService.h
//  Bond_Trading_System
//
//  Created by Huiyou Chen on 12/5/16.
//  Copyright © 2016 Francis Chen. All rights reserved.
//

#ifndef BondTradeBookingService_hpp
#define BondTradeBookingService_hpp
#include "TradeBookingService.hpp"
#include "Products.hpp"

class BondTradeBookingService: public TradeBookingService<Bond>
{
public:
    BondTradeBookingService() = default;  //default constructor
    ~BondTradeBookingService(); //destructor

    //virtual functions override
    Trade<Bond>& GetData(string key) override;
    void OnMessage(Trade<Bond>& data) override;
    void AddListener(ServiceListener<Trade<Bond> > *listener) override;
    const vector<ServiceListener<Trade<Bond> >*>& GetListeners() const override;

    //Book the trade
    void BookTrade(const Trade<Bond>& trade) override;

    //operator overload
    BondTradeBookingService & operator = (BondTradeBookingService &source);
    friend ostream& operator<<(ostream &output, const BondTradeBookingService &source);
    
private:
    map<string, Trade<Bond> > _tradingBook;
    vector<ServiceListener<Trade<Bond> >*> _tradingListeners;

};
#endif /* BondTradeBookingService_h */
