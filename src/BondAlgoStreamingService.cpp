//
//  BondAlgoStreamingService.cpp
//  Bond_Trading_System
//
//  Created by Huiyou Chen on 12/18/16.
//  Copyright © 2016 Francis Chen. All rights reserved.
//

#include "BondAlgoStreamingService.hpp"

void BondAlgoStreamingService::PublishPrice(const PriceStream<Bond>& priceStream)
{
    AlgoStream tmp(priceStream);
    this->OnMessage(tmp);
}

BondAlgoStreamingListener::BondAlgoStreamingListener(BondPricingService &prcsrv,
                                                     BondAlgoStreamingService &algostrsrv)
{
    prcsrv.AddListener(this);
    algostrsrv_ptr = &algostrsrv;
}

void BondAlgoStreamingListener::ProcessAdd(Price<Bond> &data)
{
    // Construct a PriceStream
    PriceStreamOrder bidorder(data.GetMidPrice() - data.GetSpread() * 0.5, 0, 0, BID);
    PriceStreamOrder offerorder(data.GetMidPrice() + data.GetSpread() * 0.5, 0, 0, OFFER);
    algostrsrv_ptr->PublishPrice({data.GetProduct(), bidorder, offerorder});
}


AlgoStream::AlgoStream(const PriceStream<Bond>& source): _priceStream(&source){
}

AlgoStream::AlgoStream(Bond& bond, PriceStreamOrder& bid, PriceStreamOrder& offer): PriceStream(bond, bid, offer){
}

const PriceStream<Bond>& AlgoStream::GetAlgoStream() const{
    return *_priceStream;
}

AlgoStream& BondAlgoStreamingService::GetData(string key){
    if(_algoStream.find(key)!=_algoStream.end())
        return _algoStream[key];
    else {
        cout << "No Algo Stream Data!" << endl;
        exit(-1);
    }
}

void BondAlgoStreamingService::OnMessage(AlgoStream& data){
    _algoStream[data.GetProduct().GetProductId()] = data;
    for(auto iter : _algoStreamListeners)
        iter->ProcessAdd(data);
}

void BondAlgoStreamingService::AddListener(ServiceListener<AlgoStream> *listener){
    _algoStreamListeners.push_back(listener);
}

const vector<ServiceListener<AlgoStream>*>& BondAlgoStreamingService::GetListeners() const{
    return _algoStreamListeners;
}
