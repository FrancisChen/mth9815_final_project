//
//  BondAlgoExecutionService.cpp
//  Bond_Trading_System
//
//  Created by Huiyou Chen on 12/18/16.
//  Copyright © 2016 Francis Chen. All rights reserved.
//

#include "BondAlgoExecutionService.hpp"
size_t AlgoExecution::_algoExecutionCount = 0;

AlgoExecution::AlgoExecution(const string& productId, PricingSide side, double price, double quantity): ExecutionOrder(Bond(productId), side, "AlgoOrder #"+std::to_string(++_algoExecutionCount), LIMIT, price, quantity/2, quantity/2, "NULL", false)
{

}


ExecutionOrder<Bond>* AlgoExecution::GetExecutionOrder() const{
    return _executionOrder;
}

AlgoExecution& BondAlgoExecutionService::GetData(string key){
    if(_algoExecution.find(key)!=_algoExecution.end())
        return _algoExecution[key];
    else {
        cout << "No Algo Execution Data!" << endl;
        exit(-1);
    }
}

void BondAlgoExecutionService::OnMessage(AlgoExecution& data){
    _algoExecution[data.GetProduct().GetProductId()] = data;
    for(auto iter : _algoExecutionListeners)
        iter->ProcessAdd(data);
}

void BondAlgoExecutionService::AddListener(ServiceListener<AlgoExecution> *listener){
    _algoExecutionListeners.push_back(listener);
}

const vector<ServiceListener<AlgoExecution>*>& BondAlgoExecutionService::GetListeners() const{
    return _algoExecutionListeners;
}

void BondAlgoExecutionService::AddOrderBook(OrderBook<Bond> orderbook)
{
    auto bid = AlgoExecution(orderbook.GetProduct().GetProductId(),
                             orderbook.GetBidStack().front().GetSide(),
                             orderbook.GetBidStack().front().GetPrice(),
                             orderbook.GetBidStack().front().GetQuantity() / 1000);
    auto offer = AlgoExecution(orderbook.GetProduct().GetProductId(),
                               orderbook.GetOfferStack().front().GetSide(),
                               orderbook.GetOfferStack().front().GetPrice(),
                               orderbook.GetOfferStack().front().GetQuantity() / 1000);
    this->OnMessage(bid);
    this->OnMessage(offer);
}

BondAlgoExcutionListener::BondAlgoExcutionListener(BondMarketDataService &marketService, BondAlgoExecutionService &algoService){
    marketService.AddListener(this);
    _algoExecutionService = &algoService;
}
void BondAlgoExcutionListener::ProcessAdd(OrderBook<Bond> &data){
    _algoExecutionService->AddOrderBook(data);
}


