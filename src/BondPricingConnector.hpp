//
//  BondPricingConnector.hpp
//  Bond_Trading_System
//
//  Created by Huiyou Chen on 12/17/16.
//  Copyright © 2016 Francis Chen. All rights reserved.
//

#ifndef BondPricingConnector_hpp
#define BondPricingConnector_hpp
#include "SOA.hpp"
#include "BondPricingService.hpp"
#include "Products.hpp"
#include "DataTranslator.hpp"
#include <fstream>
#include <sstream>

class BondPricingConnector: public Connector<Price<Bond> >
{
private:
    BondPricingService* _bondPricingService;
public:
    BondPricingConnector() = default;
    ~BondPricingConnector() = default;
    BondPricingConnector(BondPricingService& priceService){
        _bondPricingService = &priceService;
    };
    // override connector FileReader and Publish
    void FileReader(string& filename) override;
    void Publish(const Price<Bond> & data) override {};
};

void BondPricingConnector::FileReader(string & filename){
    ifstream pFile(filename);
    string line;
    PriceData mydata;
    if(pFile.is_open())
    {
        while(!pFile.eof()){
            getline(pFile, line);
            mydata = GetBondPrice(line);
            Price<Bond> bond_price(Bond(mydata.cusip), mydata.mid, mydata.spread);
            _bondPricingService->OnMessage(bond_price);
        }
    }
    pFile.close();
}


#endif /* BondPricingConnector_hpp */
