//
//  BondPricingService.h
//  Bond_Trading_System
//
//  Created by Huiyou Chen on 12/5/16.
//  Copyright © 2016 Francis Chen. All rights reserved.
//

#ifndef BondPricingService_hpp
#define BondPricingService_hpp
#include <string>
#include "PricingService.hpp"


class BondPricingService: public PricingService<Bond>
{
public:
    BondPricingService() = default;  //default constructor
    BondPricingService(const BondPricingService &source);  //copy constructor
    ~BondPricingService();

    //virtual functions override
    Price<Bond>& GetData(string key) override;
    void OnMessage(Price<Bond>& data) override;
    void AddListener(ServiceListener<Price<Bond> > *listener) override;
    const vector<ServiceListener<Price<Bond> >*>& GetListeners() const override;

    //operator overload
    BondPricingService & operator = (const BondPricingService &source);
    friend ostream& operator<<(ostream &output, const BondPricingService &source);

    //Publish function
    //void PublishPrice(AlgoStream<Bond> &data);

private:
    map<string, Price<Bond> > _price;
    vector<ServiceListener<Price<Bond> >*> _priceListeners;
};
#endif /* BondPricingService_h */

