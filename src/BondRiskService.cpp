//
//  BondRiskService.cpp
//  Bond_Trading_System
//
//  Created by Huiyou Chen on 12/17/16.
//  Copyright © 2016 Francis Chen. All rights reserved.
//

#include "BondRiskService.hpp"
PV01<Bond>& BondRiskService::GetData(string key){
    if(_risk.find(key) == _risk.end()){
        cout << "No Risk Data" << endl;
        exit(-1);
    }
    else return _risk[key];
}

void BondRiskService::OnMessage(PV01<Bond>& data){
    _risk[data.GetProduct().GetProductId()] = data;
    for(auto iter : _riskListeners)
        iter->ProcessAdd(data);
}

void BondRiskService::AddListener(ServiceListener<PV01<Bond> > *listener){
    _riskListeners.push_back(listener);
}

const vector<ServiceListener<PV01<Bond> >*>& BondRiskService::GetListeners() const{
    return _riskListeners;
}

void BondRiskService::AddPosition(Position<Bond> &position){
    string key = position.GetProduct().GetProductId();
    if(_risk.find(key)!=_risk.end()){
        _risk[key].UpdateQuantity(position);
    }
    else{
        PV01<Bond> new_pv01(position.GetProduct(), BondPV01(key), position.GetAggregatePosition());
        _risk[key] = new_pv01;
    }
}

double BondRiskService::BondPV01(string key) const{
    if (key == "912828A75") return 0.01;
    else if (key == "912828G95") return 0.01974732;
    else if (key == "912828M98") return 0.02333471;
    else if (key == "912828G87") return 0.01587385;
    else if (key == "912828U24") return 0.02387587;
    else if (key == "912810RU4") return 0.07124954;
    return 0.05928573;
    
//    912828A75, 1.500%, 12/31/2018
//    912828G95, 1.625%, 12/31/2019
//    912828G87, 2.125%, 12/31/2021
//    912828U57, 2.125%, 11/30/2023
//    912828U24, 2.000%, 11/15/2026
//    912810RU4, 2.875%, 11/15/2046

}

const double BondRiskService::GetBucketedRisk(const BucketedSector<Bond> &sector) const{
    double bucketRisk = 0.0;
    for(auto bucket : sector.GetProducts()){
        string key = bucket.GetProductId();
        if(_risk.find(key) != _risk.end())
            bucketRisk += _risk.at(key).GetPV01();
        else
            bucketRisk += BondPV01(bucket.GetProductId());
    }
    return bucketRisk;
}

double BondRiskService::GetRisk(const Bond &bond)
{
    auto pv01 = GetData(bond.GetProductId());
    return pv01.GetPV01() * pv01.GetQuantity();
}

ostream& operator<<(ostream &output, const BondRiskService &source){
    for(auto iter : source._risk){
        output << iter.second.GetProduct().GetProductId() << ", " << iter.second.GetQuantity()<< ", " << iter.second.GetQuantity() << "\n";
    }
    return output;
}
