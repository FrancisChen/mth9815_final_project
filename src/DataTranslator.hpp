//
//  DataTranslator.hpp
//  Bond_Trading_System
//
//  Created by Huiyou Chen on 12/5/16.
//  Copyright © 2016 Francis Chen. All rights reserved.
//

#ifndef DataTranslator_hpp
#define DataTranslator_hpp
#include "Products.hpp"
#include "TradeBookingService.hpp"
#include "InquiryService.hpp"
#include "MarketDataService.hpp"
#include <tuple>
#include <string>
#include <sstream>
#include <fstream>
#include <iostream>
#include <vector>
#include <map>
#include "boost/date_time/gregorian/gregorian.hpp"
using namespace std;

//enum Book { TRSY1, TRSY2, TRSY3 };
//enum Side { BUY, SELL };
//enum InquiryState { RECEIVED, QUOTED, DONE, REJECTED, CUSTOMER_REJECTED };
//enum Market { BROKERTEC, ESPEED, CME };
const std::vector<string> Book{"TRSY1", "TRSY2", "TRSY3"};
date DateTranslator(string& str);

double PriceTranslator(string& str);

string PriceToString(double dprice);

double SpreadTranslator(string& str);

string BookTranslator(string& str);

Side SideTranslator(string& str);

InquiryState StateTranslator(string& str);

struct TradeData{
    string cusip;
    string ticker;
    string book;
    long quantity;
    Side side;
};

/*
 912828A75, 1.500%, 12/31/2018
 912828G95, 1.625%, 12/31/2019
 912828G87, 2.125%, 12/31/2021
 912828U57, 2.125%, 11/30/2023
 912828U24, 2.000%, 11/15/2026
 912810RU4, 2.875%, 11/15/2046
 */


TradeData GetTradeData(string& ss);

struct PriceData{
    string cusip;
    double mid;
    double spread;
};

PriceData GetBondPrice(string &ss);

struct MarketData{
    string cusip;
    vector<Order> bidStack;
    vector<Order> offerStack;
};
MarketData GetMarketData(string& ss);


struct InquiryData{
    string inquiryId;
    string cusip;
    Side side;
    long quantity;
    double price;
    InquiryState state;
};

InquiryData GetInquiryData(string &ss);

class FileOutput{
public:
    FileOutput(const string& filePath);
    virtual ~FileOutput();
    void OutputLine(const vector<string>& lines);
private:
    string _filePath;
    ofstream _output;
};


#endif /* DataTranslator_hpp */
