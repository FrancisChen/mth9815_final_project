//
//  BondMarketDataConnector.hpp
//  Bond_Trading_System
//
//  Created by Huiyou Chen on 12/18/16.
//  Copyright © 2016 Francis Chen. All rights reserved.
//

#ifndef BondMarketDataConnector_hpp
#define BondMarketDataConnector_hpp
#include "SOA.hpp"
#include "BondMarketDataService.hpp"
#include "Products.hpp"
#include "DataTranslator.hpp"
#include <fstream>
#include <sstream>

class BondMarketDataConnector: public Connector<OrderBook<Bond> >
{
private:
    BondMarketDataService* _marketDataService;
public:
    BondMarketDataConnector() = default;
    virtual ~BondMarketDataConnector() = default;
    BondMarketDataConnector(BondMarketDataService& marketDataService){
        _marketDataService = &marketDataService;
    };
    void AddOrderBook(OrderBook<Bond> orderbook)
    {
        _marketDataService->OnMessage(orderbook);
    }
    // override connector FileReader and Publish
    void FileReader(string& filename) override;
    void Publish(const OrderBook<Bond> & data) override {};
};

void BondMarketDataConnector::FileReader(string& filename){
    ifstream pFile(filename);
    string line;
    MarketData mydata;
    if(pFile.is_open())
    {
        while(!pFile.eof()){
            getline(pFile, line);
            mydata = GetMarketData(line);
            OrderBook<Bond> orderbook(Bond(mydata.cusip), mydata.bidStack, mydata.offerStack);
            _marketDataService->OnMessage(orderbook);
        }
    }
    pFile.close();
}

#endif /* BondMarketDataConnector_hpp */
