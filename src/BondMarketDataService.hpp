//
//  BondMarketDataService.h
//  Bond_Trading_System
//
//  Created by Huiyou Chen on 12/5/16.
//  Copyright © 2016 Francis Chen. All rights reserved.
//

#ifndef BondMarketDataService_hpp
#define BondMarketDataService_hpp
#include "SOA.hpp"
#include "Products.hpp"
#include "MarketDataService.hpp"

class BondMarketDataService: public MarketDataService<Bond>
{
public:
    BondMarketDataService() = default;

    virtual ~BondMarketDataService() = default;

    // Get the best bid/offer order
    const BidOffer GetBestBidOffer(const string &productId) override;

    // Aggregate the order book
    const long AggregateDepth(const string &productId, const double price) override;

    OrderBook<Bond>& GetData(string key) override;

    void OnMessage(OrderBook<Bond>& data) override;

    void AddListener(ServiceListener<OrderBook<Bond> > *listener) override;

    const vector<ServiceListener<OrderBook<Bond> >*>& GetListeners() const override;

    //friend ostream& operator<<(ostream &output, const  BondMarketDataService &source);

private:
    map<string, OrderBook<Bond> > _marketData;
    vector<ServiceListener<OrderBook<Bond> >*> _marketDataListeners;
};
#endif /* BondMarketDataService_h */
