//
//  BondExecutionListener.cpp
//  Bond_Trading_System
//
//  Created by Huiyou Chen on 12/18/16.
//  Copyright © 2016 Francis Chen. All rights reserved.
//

#include "BondExecutionListener.hpp"
#include <cstdlib>
BondExecutionListener::BondExecutionListener(BondAlgoExecutionService &algoService, BondExecutionService & executionService)
{
    algoService.AddListener(this);
    _bondExecutionService = &executionService;
}

void BondExecutionListener::ProcessAdd(AlgoExecution &data)
{
    // Put the order in a random market
    _bondExecutionService->ExecuteOrder(*data.GetExecutionOrder(), Market(rand()%3));
}
