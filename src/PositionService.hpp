/**
 * positionservice.hpp
 * Defines the data types and Service for positions.
 *
 * @author Breman Thuraisingham
 * Implemented by Huiyou Chen on 12/5/16.
 * Copyright © 2016 Francis Chen. All rights reserved.
 */
#ifndef POSITION_SERVICE_HPP
#define POSITION_SERVICE_HPP

#include <string>
#include <map>
#include "SOA.hpp"
#include "TradeBookingService.hpp"

using namespace std;

/**
 * Position class in a particular book.
 * Type T is the product type.
 */
template<typename T>
class Position
{

public:

  // ctor for a position
  Position();
  Position(const T &product);
  virtual ~Position() = default;
  // Get the product
  const T& GetProduct() const;

  // Get the position quantity
  long GetPosition(const string &book);
  long GetPosition(const Trade<T>& _trade);

  // Get the aggregate position
  long GetAggregatePosition();

  //Add position
  void AddPosition(string &book, long quantitiy);

private:
  T _product;
  map<string,long> _positions;

};

template<typename T>
Position<T>::Position(){
    _positions["TRSY1"] = 0;
    _positions["TRSY2"] = 0;
    _positions["TRSY3"] = 0;
}
template<typename T>
Position<T>::Position(const T &product) : _product(product)
{
    _positions["TRSY1"] = 0;
    _positions["TRSY2"] = 0;
    _positions["TRSY3"] = 0;
}

template<typename T>
const T& Position<T>::GetProduct() const
{
    return _product;
}

template<typename T>
long Position<T>::GetPosition(const string &book)
{
    return _positions[book];
}

template<typename T>
long Position<T>::GetPosition(const Trade<T>& _trade)
{
    return _positions[_trade.GetBook()];
}

template<typename T>
long Position<T>::GetAggregatePosition()
{
    // No-op implementation - should be filled out for implementations
    long aggregate_position(0);
    for(auto iter : _positions)
        aggregate_position += iter.second;
    return aggregate_position;
}

template<typename T>
void Position<T>::AddPosition(string &book, long quantitiy)
{
    if(_positions.find(book)==_positions.end()) _positions[book] = quantitiy;
    else _positions[book] += quantitiy;
}

/**
 * Position Service to manage positions across multiple books and secruties.
 * Keyed on product identifier.
 * Type T is the product type.
 */
template<typename T>
class PositionService : public Service<string,Position<T> >
{

public:

    // Add a trade to the service
    virtual void AddTrade(const Trade<T> &trade) = 0;
    //virtual void RemoveTrade(const Trade<T>& trade) = 0;
    //virtual void UpdateTrade(const Trade<T>& trade) = 0;

};


#endif
