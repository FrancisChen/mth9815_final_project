/**
 * tradebookingservice.hpp
 * Defines the data types and Service for trade booking.
 *
 * @author Breman Thuraisingham
 * Implemented by Huiyou Chen on 12/5/16.
 * Copyright © 2016 Francis Chen. All rights reserved.
 */

#ifndef TRADE_BOOKING_SERVICE_HPP
#define TRADE_BOOKING_SERVICE_HPP

#include <string>
#include <vector>
#include "SOA.hpp"

// Trade sides
enum Side { BUY, SELL };

/**
 * Trade object with a price, side, and quantity on a particular book.
 * Type T is the product type.
 */
template<typename T>
class Trade
{

public:

  // ctor for a trade
  Trade() = default;

  Trade(const T& _product, string _tradeId, string _book, long _quantity, Side _side);

  // Get the product
  const T& GetProduct() const;

  // Get the trade ID
  const string& GetTradeId() const;

  // Get the book
  const string& GetBook() const;

  // Get the quantity
  long GetQuantity() const;

  // Get the side
  Side GetSide() const;

  Trade<T>& operator = (const Trade<T>& trade);

private:
  T product;
  string tradeId;
  string book;
  long quantity;
  Side side;

};

/**
 * Trade Booking Service to book trades to a particular book.
 * Keyed on product identifier.
 * Type T is the product type.
 */
template<typename T>
class TradeBookingService : public Service<string,Trade <T> >
{
public:
    TradeBookingService() = default;
    // Book the trade
    virtual void BookTrade(const Trade<T> &trade) = 0;
};

template<typename T>
Trade<T>::Trade(const T& _product, string _tradeId, string _book, long _quantity, Side _side) :
product(_product)
{
    tradeId = _tradeId;
    book = _book;
    quantity = _quantity;
    side = _side;
}

template<typename T>
const T& Trade<T>::GetProduct() const
{
    return product;
}

template<typename T>
const string& Trade<T>::GetTradeId() const
{
    return tradeId;
}

template<typename T>
const string& Trade<T>::GetBook() const
{
    return book;
}

template<typename T>
long Trade<T>::GetQuantity() const
{
    return quantity;
}

template<typename T>
Side Trade<T>::GetSide() const
{
    return side;
}
template<typename T>
Trade<T>& Trade<T>::operator = (const Trade<T>& trade){
    if(&trade == this) return *this;
    product = trade.GetProduct();
    tradeId = trade.GetTradeId();
    book = trade.GetBook();
    quantity = trade.GetQuantity();
    side = GetSide();
    return *this;
}


#endif
