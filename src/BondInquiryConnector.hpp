//
//  BondInquiryService.h
//  Bond_Trading_System
//
//  Created by Huiyou Chen on 12/18/16.
//  Copyright © 2016 Francis Chen. All rights reserved.
//

#ifndef BondInquiryConnector_hpp
#define BondInquiryConnector_hpp
#include "BondInquiryService.hpp"
#include "DataTranslator.hpp"
#include "SOA.hpp"
#include "Products.hpp"
#include <fstream>
#include <sstream>

class BondInquiryConnector : public Connector<Inquiry<Bond> >
{
private:
    BondInquiryService* _bondInquiryService;
public:
    BondInquiryConnector() = default;
    ~BondInquiryConnector() = default;
    BondInquiryConnector(BondInquiryService& source){
        _bondInquiryService = &source;
    }
    // override connector FileReader and Publish
    void FileReader(string& filename) override;
    void Publish(const Inquiry<Bond> & data) override;
};

void BondInquiryConnector::FileReader(string & filename){
    ifstream pFile(filename);
    string line;
    InquiryData mydata;
    if(pFile.is_open())
    {
        while(!pFile.eof()){
            getline(pFile, line);
            mydata = GetInquiryData(line);
            Inquiry<Bond> inquiry(mydata.inquiryId, Bond(mydata.cusip), mydata.side, mydata.quantity, mydata.price, mydata.state);
            _bondInquiryService->OnMessage(inquiry);
        }
    }
    pFile.close();
}

void BondInquiryConnector::Publish(const Inquiry<Bond> & data){

}


#endif /* BondInquiryConnector_hpp */
