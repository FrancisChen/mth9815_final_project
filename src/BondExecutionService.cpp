//
//  BondExecutionService.cpp
//  Bond_Trading_System
//
//  Created by Huiyou Chen on 12/18/16.
//  Copyright © 2016 Francis Chen. All rights reserved.
//

#include "BondExecutionService.hpp"

ExecutionOrder<Bond>& BondExecutionService::GetData(string key){
    if(_execution.find(key)!=_execution.end())
        return _execution[key];
    else {
        cout << "No Execution Data!" << endl;
        exit(-1);
    }
}

void BondExecutionService::OnMessage(ExecutionOrder<Bond>& data){
    _execution[data.GetProduct().GetProductId()] = data;
    for(auto iter : _executionListeners)
        iter->ProcessAdd(data);
}

void BondExecutionService::AddListener(ServiceListener<ExecutionOrder<Bond> > *listener){
    _executionListeners.push_back(listener);
}

const vector<ServiceListener<ExecutionOrder<Bond> >*>& BondExecutionService::GetListeners() const{
    return _executionListeners;
}

// Execute an order on a market
void BondExecutionService::ExecuteOrder(ExecutionOrder<Bond>& order, Market market){
    cout << order.GetOrderId() << ", " << order.GetPrice() << ", " << order.GetVisibleQuantity() << ", " << market << endl;
    this->BondExecutionService::OnMessage(order);
}

ostream& operator<<(ostream &output, const BondExecutionService &source){
    for(auto iter : source._execution){
        output << iter.second.GetOrderId() << ", " << iter.second.GetOrderType() << ", " <<
        iter.second.GetProduct().GetProductId() << ", " << iter.second.GetPrice() << ", " <<
        iter.second.GetVisibleQuantity()<< ", " << iter.second.GetHiddenQuantity() << "\n";
    }
    return output;
}
