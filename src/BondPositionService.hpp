//
//  BondPositionService.h
//  Bond_Trading_System
//
//  Created by Huiyou Chen on 12/5/16.
//  Copyright © 2016 Francis Chen. All rights reserved.
//

#ifndef BondPositionService_hpp
#define BondPositionService_hpp
#include "PositionService.hpp"
#include "Products.hpp"

class BondPositionService: public PositionService<Bond>
{
public:
    BondPositionService() = default;
    virtual ~BondPositionService() = default;

    void AddTrade(const Trade<Bond>& trade) override;
    //void ProcessAdd(Trade<Bond>& data); listener

    Position<Bond>& GetData(string key) override;
    void OnMessage(Position<Bond>& data) override;
    void AddListener(ServiceListener<Position<Bond> > *listener) override;
    const vector< ServiceListener<Position<Bond> >*>& GetListeners() const override;

    friend ostream& operator<<(ostream &output, const BondPositionService &source);
private:
    map<string, Position<Bond> > _position;
    vector<ServiceListener<Position<Bond> >*> _positionListeners;
};

#endif /* BondPositionService_h */
