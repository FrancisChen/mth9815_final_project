//
//  BondRiskListener.cpp
//  Bond_Trading_System
//
//  Created by Huiyou Chen on 12/18/16.
//  Copyright © 2016 Francis Chen. All rights reserved.
//

#include "BondRiskListener.hpp"
BondRiskListener::BondRiskListener(BondPositionService& positionService, BondRiskService& riskService){
    positionService.AddListener(this);
    _riskService = &riskService;
}

void BondRiskListener::ProcessAdd(Position<Bond> &data){
    _riskService->AddPosition(data);
}

void BondRiskListener::AddReceiver(BondRiskService& receiver){
    _riskService = &receiver;
}

