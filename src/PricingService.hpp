/**
 * pricingservice.hpp
 * Defines the data types and Service for internal prices.
 *
 * @author Breman Thuraisingham
 * Implemented by Huiyou Chen on 12/5/16.
 * Copyright © 2016 Francis Chen. All rights reserved.
 */
#ifndef PRICING_SERVICE_HPP
#define PRICING_SERVICE_HPP

#include <string>
#include "SOA.hpp"
#include "Products.hpp"

/**
 * A price object consisting of mid and bid/offer spread.
 * Type T is the product type.
 */
template<class T>
class Price
{
public:
    // ctor for a price
    Price() = default;
    Price(const T & product, double mid, double spread);
    virtual ~Price() = default;

    // Get the product
    const T& GetProduct() const;

    // Get the mid price
    double GetMidPrice() const;

    // Get the bid/offer spread around the mid
    double GetSpread() const;

    //operator overload
    Price<T>& operator = (const Price<T>& source);
    friend ostream& operator<<(ostream &output, const Price<T> &source);

private:
    T _product;
    double _midPrice;
    double _bidofferSpread;

};

/**
 * Pricing Service managing mid prices and bid/offers.
 * Keyed on product identifier.
 * Type T is the product type.
 */
template<class T>
class PricingService : public Service<string,Price<T> >
{
public:
//    PricingService() = default;
//    ~PricingService(){
//        ;
private:

};

template<class T>
Price<T>::Price(const T & product, double mid, double spread): _product(product), _midPrice(mid), _bidofferSpread(spread){

}

template<class T>
const T& Price<T>::GetProduct() const{
    return _product;
}

template<class T>
double Price<T>::GetMidPrice() const{
    return _midPrice;
}

template<class T>
double Price<T>::GetSpread() const{
    return _bidofferSpread;
}

template<class T>
Price<T>& Price<T>::operator = (const Price<T>& source){
    if(&source == this) return *this;
    _product = source._product;
    _midPrice = source._midPrice;
    _bidofferSpread = source._bidofferSpread;
    return *this;
}

template<class T>
ostream& operator<<(ostream& output, const Price<T> &source){
    output << source.GetMidPrice() << ", " << source.GetSpread();
    return output;
}


#endif
