//
//  BondRiskService.h
//  Bond_Trading_System
//
//  Created by Huiyou Chen on 12/5/16.
//  Copyright © 2016 Francis Chen. All rights reserved.
//

#ifndef BondRiskService_hpp
#define BondRiskService_hpp
#include "SOA.hpp"
#include "PositionService.hpp"
#include "RiskService.hpp"
#include "Products.hpp"
#include <vector>
#include <map>

class BondRiskService: public RiskService<Bond>
{
public:
    //ctors
    BondRiskService() = default;
    virtual ~BondRiskService() = default;

    //override
    PV01<Bond>& GetData(string key) override;
    void OnMessage(PV01<Bond>& data) override;
    void AddListener(ServiceListener<PV01<Bond> > *listener) override;
    const vector< ServiceListener<PV01<Bond> > *>& GetListeners() const override;

    void AddPosition(Position<Bond> &position) override;
    const double GetBucketedRisk(const BucketedSector<Bond> &sector) const override;
    double GetRisk(const Bond &bond);
    double BondPV01(string key) const;
    friend ostream& operator<<(ostream &output, const BondRiskService &source);

private:
    map<string, PV01<Bond> > _risk;
    vector<ServiceListener<PV01<Bond> >*> _riskListeners;
    
};
#endif /* BondRiskService_h */
