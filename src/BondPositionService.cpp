//
//  BondPositionService.cpp
//  Bond_Trading_System
//
//  Created by Huiyou Chen on 12/17/16.
//  Copyright © 2016 Francis Chen. All rights reserved.
//

#include "BondPositionService.hpp"
void BondPositionService::AddTrade(const Trade<Bond> &trade)
{
    string key = trade.GetProduct().GetProductId();
    string book = trade.GetBook();
    long quantity = trade.GetQuantity();
    if(trade.GetSide() == Side::SELL) _position[key].AddPosition(book, -quantity);
    else _position[key].AddPosition(book, quantity);
    _positionListeners[0]->ProcessAdd(_position[key]);
}

Position<Bond>& BondPositionService::GetData(string key){
    if(_position.find(key)!=_position.end()){
        cout << "Not Position Data" << endl;
        exit(-1);
    }
    else return _position[key];
}

void BondPositionService::OnMessage(Position<Bond>& data){
    _position[data.GetProduct().GetProductId()] = data;
    for(auto iter : _positionListeners)
        iter->ProcessAdd(data);
}

void BondPositionService::AddListener(ServiceListener<Position<Bond> > *listener)
{
    _positionListeners.push_back(listener);
}

const vector<ServiceListener<Position<Bond> >*>& BondPositionService::GetListeners() const{
    return _positionListeners;
}

ostream& operator<<(ostream &output, const BondPositionService &source){
    for(auto iter : source._position){
        output << iter.second.GetProduct().GetProductId() << ", " << iter.second.GetAggregatePosition() << "\n";
    }
    return output;
}


