//
//  BondStreamingListener.hpp
//  Bond_Trading_System
//
//  Created by Huiyou Chen on 12/18/16.
//  Copyright © 2016 Francis Chen. All rights reserved.
//

#ifndef BondStreamingListener_hpp
#define BondStreamingListener_hpp

#include "HistoricalDataService.hpp"
#include "BondAlgoStreamingService.hpp"
#include "BondStreamingService.hpp"

class BondStreamingListener : public ServiceListener<AlgoStream>
{
public:
    BondStreamingListener(BondAlgoStreamingService & algoService,
                          BondStreamingService & streamService);
    void ProcessAdd(AlgoStream& data) override;
    void ProcessRemove(AlgoStream& data) override {};
    void ProcessUpdate(AlgoStream& data) override {};
private:
    BondStreamingService * _bondStreamService;
};


#endif /* BondStreamingListener_hpp */
