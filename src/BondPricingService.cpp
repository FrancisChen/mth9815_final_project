//
//  BondPricingService.cpp
//  Bond_Trading_System
//
//  Created by Huiyou Chen on 12/16/16.
//  Copyright © 2016 Francis Chen. All rights reserved.
//

#include "BondPricingService.hpp"
BondPricingService::BondPricingService(const BondPricingService &source): _price(source._price), _priceListeners(source._priceListeners){

}

BondPricingService::~BondPricingService(){

}

Price<Bond>& BondPricingService::GetData(string key){
    if(_price.find(key) == _price.end()){
        cout << "No Price Data!" << endl;
        exit(-1);
    }
    else return _price[key];
}

void BondPricingService::OnMessage(Price<Bond>& data){
    _price[data.GetProduct().GetProductId()] = data;
    for(auto iter : _priceListeners){
        iter->ProcessAdd(data);
    }
}

void BondPricingService::AddListener(ServiceListener<Price<Bond> > *listener){
    _priceListeners.push_back(listener);
}

const vector<ServiceListener<Price<Bond> >*>& BondPricingService::GetListeners() const{
    return _priceListeners;
}

BondPricingService& BondPricingService::operator = (const BondPricingService& source){
    if(&source == this) return *this;
    for(auto iter : source._price){
        _price.insert(std::pair<string,Price<Bond> >(iter.first,iter.second));
    }
    _priceListeners = source._priceListeners;
    return *this;
}

ostream& operator<<(ostream &output, const BondPricingService &source){
    for(auto iter : source._price){
        output << iter.second.GetProduct().GetProductId() << ", " << iter.second.GetMidPrice() << iter.second.GetSpread() << "\n";
    }
    return output;
}
