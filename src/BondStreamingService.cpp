//
//  BondStreamingService.cpp
//  Bond_Trading_System
//
//  Created by Huiyou Chen on 12/18/16.
//  Copyright © 2016 Francis Chen. All rights reserved.
//

#include "BondStreamingService.hpp"
BondStreamingService::BondStreamingService(string filePath): fileWriter(filePath){

}

PriceStream<Bond>& BondStreamingService::GetData(string key){
    if(_stream.find(key)!=_stream.end())
        return _stream[key];
    else {
        cout << "No Execution Data!" << endl;
        exit(-1);
    }
}

void BondStreamingService::OnMessage(PriceStream<Bond>& data){
    _stream[data.GetProduct().GetProductId()] = data;
    for(auto iter : _streamListeners)
        iter->ProcessAdd(data);
}

void BondStreamingService::AddListener(ServiceListener<PriceStream<Bond> > *listener){
    _streamListeners.push_back(listener);
}

const vector<ServiceListener<PriceStream<Bond> >*>& BondStreamingService::GetListeners() const{
    return _streamListeners;
}

void BondStreamingService::PublishPrice(const PriceStream<Bond>& _priceStream){
    fileWriter.Publish(_priceStream);
    auto tmp = _priceStream;
    this->OnMessage(tmp);
}

ostream& operator<<(ostream &output, const BondStreamingService &source){
    for(auto iter : source._stream){
        output << iter.second.GetProduct().GetProductId() << ", " << iter.second.GetBidOrder().GetPrice() << ", " << iter.second.GetBidOrder().GetVisibleQuantity() << ", " << iter.second.GetBidOrder().GetHiddenQuantity() << ", " << iter.second.GetOfferOrder().GetPrice() << ", " << iter.second.GetOfferOrder().GetVisibleQuantity() << ", " << iter.second.GetOfferOrder().GetHiddenQuantity() <<  "\n";
    }
    return output;
}
