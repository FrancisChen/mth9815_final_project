//
//  BondStreamingService.h
//  Bond_Trading_System
//
//  Created by Huiyou Chen on 12/5/16.
//  Copyright © 2016 Francis Chen. All rights reserved.
//

#ifndef BondStreamingService_hpp
#define BondStreamingService_hpp
#include "SOA.hpp"
#include "Products.hpp"
#include "StreamingService.hpp"
#include "DataTranslator.hpp"
#include "BondStreamingConnector.hpp"

class BondStreamingService : public StreamingService<Bond>
{
public:
    BondStreamingService() = default;

    virtual ~BondStreamingService() = default;

    BondStreamingService(string filePath);

    PriceStream<Bond>& GetData(string key) override;

    void OnMessage(PriceStream<Bond>& data) override;

    void AddListener(ServiceListener<PriceStream<Bond> > *listener) override;

    const vector<ServiceListener<PriceStream<Bond> >*>& GetListeners() const override;

    void PublishPrice(const PriceStream<Bond>& _priceStream) override;

    friend ostream& operator<<(ostream &output, const BondStreamingService &source);
private:
    map<string, PriceStream<Bond> > _stream;
    vector<ServiceListener<PriceStream<Bond> >*> _streamListeners;
    BondStreamingConnector fileWriter;
};

//class BondStreamingListener : public ServiceListener<AlgoStream>
//{
//public:
//    BondStreamingListener(BondAlgoStreamingService &algostrsrv,
//                          BondStreamingService &bstrsrv);
//    void ProcessAdd(AlgoStream& data);
//private:
//    BondStreamingService *bstrsrv_ptr;
//};

#endif /* BondStreamingService_h */
