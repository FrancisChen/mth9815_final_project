//
//  MarketDataService.cpp
//  Bond_Trading_System
//
//  Created by Huiyou Chen on 12/18/16.
//  Copyright © 2016 Francis Chen. All rights reserved.
//

#include "MarketDataService.hpp"
Order::Order(double _price, long _quantity, PricingSide _side)
{
    price = _price;
    quantity = _quantity;
    side = _side;
}

double Order::GetPrice() const
{
    return price;
}

long Order::GetQuantity() const
{
    return quantity;
}

PricingSide Order::GetSide() const
{
    return side;
}

BidOffer::BidOffer(const Order &_bidOrder, const Order &_offerOrder) :
bidOrder(_bidOrder), offerOrder(_offerOrder)
{
}

const Order& BidOffer::GetBidOrder() const
{
    return bidOrder;
}

const Order& BidOffer::GetOfferOrder() const
{
    return offerOrder;
}
