/**
 * historicaldataservice.hpp
 * historicaldataservice.hpp
 *
 * @author Breman Thuraisingham
 * Defines the data types and Service for historical data.
 *
 * Implemented by Huiyou Chen on 12/5/16.
 * Copyright © 2016 Francis Chen. All rights reserved.
 */
#ifndef HISTORICAL_DATA_SERVICE_HPP
#define HISTORICAL_DATA_SERVICE_HPP
#include "SOA.hpp"
#include "Products.hpp"
#include "PositionService.hpp"
#include "RiskService.hpp"
#include "ExecutionService.hpp"
#include "InquiryService.hpp"
#include "StreamingService.hpp"

template<typename T>
class HistoricalData{
public:
    // ctor for each type of listeners
    HistoricalData() = default;
    virtual ~HistoricalData() = default;
    HistoricalData(const string& key, double val);
    HistoricalData(PV01<T>& data);
    HistoricalData(Position<T>& data);
    HistoricalData(ExecutionOrder<T>& data);
    HistoricalData(PriceStream<T>& data);
    HistoricalData(Inquiry<T>& data);

    const std::vector<string>& GetHistoricalData() const;

private:
    std::vector<string> _historicalData;
};

/**
 * Service for processing and persisting historical data to a persistent store.
 * Keyed on some persistent key.
 * Type T is the data type to persist.
 */
template<typename T>
class HistoricalDataService : public Service<string, HistoricalData<T> >
{
public:
  // Persist data to a store
  virtual void PersistData(string persistKey, const HistoricalData<T>& data) = 0;

};

template<typename T>
HistoricalData<T>::HistoricalData(const string& key, double val)
{
    _historicalData.push_back(key);
    _historicalData.push_back(to_string(val));
}

template<typename T>
HistoricalData<T>::HistoricalData(PV01<T>& data)
{
    _historicalData.push_back(data.GetProduct().GetProductId());
    _historicalData.push_back(to_string(data.GetPV01()));
    _historicalData.push_back(to_string(data.GetQuantity()));
}

template<typename T>
HistoricalData<T>::HistoricalData(Position<T>& data)
{
    _historicalData.push_back(data.GetProduct().GetProductId());
    _historicalData.push_back(to_string(data.GetPosition("TRSY1")));
    _historicalData.push_back(to_string(data.GetPosition("TRSY2")));
    _historicalData.push_back(to_string(data.GetPosition("TRSY3")));
    _historicalData.push_back(to_string(data.GetAggregatePosition()));
}

template<typename T>
HistoricalData<T>::HistoricalData(ExecutionOrder<T>& data)
{
    _historicalData.push_back(data.GetOrderId());
    _historicalData.push_back(data.GetProduct().GetProductId());
    _historicalData.push_back(to_string(data.GetOrderType()));
    _historicalData.push_back(to_string(data.GetPrice()));
    _historicalData.push_back(to_string(data.GetVisibleQuantity()));
    _historicalData.push_back(to_string(data.GetHiddenQuantity()));
}

template<typename T>
HistoricalData<T>::HistoricalData(Inquiry<T>& data)
{
    _historicalData.push_back(data.GetInquiryId());
    _historicalData.push_back(data.GetProduct().GetProductId());
    _historicalData.push_back(to_string(data.GetSide()));
    _historicalData.push_back(to_string(data.GetPrice()));
    _historicalData.push_back(to_string(data.GetQuantity()));
    _historicalData.push_back(to_string(data.GetState()));
}

template<typename T>
HistoricalData<T>::HistoricalData(PriceStream<T>& data)
{
    _historicalData.push_back(data.GetProduct().GetProductId());
    auto bid = data.GetBidOrder();
    auto offer = data.GetOfferOrder();
    _historicalData.push_back(to_string(bid.GetSide()));
    _historicalData.push_back(to_string(bid.GetPrice()));
    _historicalData.push_back(to_string(bid.GetVisibleQuantity()));
    _historicalData.push_back(to_string(bid.GetHiddenQuantity()));
    _historicalData.push_back(to_string(offer.GetSide()));
    _historicalData.push_back(to_string(offer.GetPrice()));
    _historicalData.push_back(to_string(offer.GetVisibleQuantity()));
    _historicalData.push_back(to_string(offer.GetHiddenQuantity()));
}

template<typename T>
const vector<string>& HistoricalData<T>::GetHistoricalData() const{
    return _historicalData;
}

#endif
