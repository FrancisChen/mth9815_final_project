//
//  BondTradeBookingConnector.hpp
//  Bond_Trading_System
//
//  Created by Huiyou Chen on 12/5/16.
//  Copyright © 2016 Francis Chen. All rights reserved.
//

#ifndef BondTradeBookingConnector_hpp
#define BondTradeBookingConnector_hpp
#include "SOA.hpp"
#include "TradeBookingService.hpp"
#include "Products.hpp"
#include "BondTradeBookingService.hpp"
#include "DataTranslator.hpp"
#include <fstream>
#include <sstream>

class BondTradeBookingConnector: public Connector<Trade<Bond> >
{
private:
    BondTradeBookingService* _bondTradeBookingService;
public:
    BondTradeBookingConnector() = default;
    ~BondTradeBookingConnector() = default;
    BondTradeBookingConnector(BondTradeBookingService& source){
        _bondTradeBookingService = &source;
    }
    // override connector FileReader and Publish
    void FileReader(string& filename) override;
    void Publish(const Trade<Bond> & data) override {};
};

void BondTradeBookingConnector::FileReader(string & filename){
    ifstream pFile(filename);
    string line;
    TradeData mydata;
    if(pFile.is_open())
    {
        while(!pFile.eof()){
            getline(pFile, line);
            mydata = GetTradeData(line);
            Trade<Bond> trade(Bond(mydata.cusip), mydata.ticker, mydata.book, mydata.quantity, mydata.side);
            _bondTradeBookingService->OnMessage(trade);
        }
    }
    pFile.close();
}


#endif /* BondTradeBookingConnector_hpp */
