//
//  BondAlgoExecutionService.h
//  Bond_Trading_System
//
//  Created by Huiyou Chen on 12/5/16.
//  Copyright © 2016 Francis Chen. All rights reserved.
//

#ifndef BondAlgoExecutionService_hpp
#define BondAlgoExecutionService_hpp
#include "SOA.hpp"
#include "Products.hpp"
#include "ExecutionService.hpp"
#include "MarketDataService.hpp"
#include "BondMarketDataService.hpp"

// Algo execution base class
class AlgoExecution: public ExecutionOrder<Bond>
{
public:
    //ctors
    AlgoExecution() = default;
    virtual ~AlgoExecution() = default;
    AlgoExecution(const string& productId, PricingSide side, double price, double quantity);
    ExecutionOrder<Bond>* GetExecutionOrder() const;
    //friend ostream& operator<<(ostream& os, AlgoExecution<T>& source);
    //const AlgoExecution<T>& GetAlgoExecution() const;
private:
    static size_t _algoExecutionCount;
    ExecutionOrder<Bond> * _executionOrder;

};

class AlgoExecutionService : public Service<string, AlgoExecution>
{
    virtual void AddOrderBook(OrderBook<Bond> orderbook) = 0;
};

class BondAlgoExecutionService : public AlgoExecutionService
{
public:
    BondAlgoExecutionService() = default;

    virtual ~BondAlgoExecutionService() = default;

    AlgoExecution& GetData(string key) override;

    void OnMessage(AlgoExecution& data) override;

    void AddListener(ServiceListener<AlgoExecution> *listener) override;

    const vector<ServiceListener<AlgoExecution>*>& GetListeners() const override;

    void AddOrderBook(OrderBook<Bond> orderbook) override;

private:
    map<string, AlgoExecution> _algoExecution;
    vector<ServiceListener<AlgoExecution>*> _algoExecutionListeners;
};

class BondAlgoExcutionListener : public ServiceListener<OrderBook<Bond> >
{
public:
    BondAlgoExcutionListener(BondMarketDataService &marketService,
                             BondAlgoExecutionService &algoService);
    void ProcessAdd(OrderBook<Bond> &data) override;
    void ProcessRemove(OrderBook<Bond> &data) override {};
    void ProcessUpdate(OrderBook<Bond> &data) override {};
private:
    BondAlgoExecutionService *_algoExecutionService;
};



#endif /* BondAlgoExecutionService_h */
