//
//  BondHistoricalDataService.h
//  Bond_Trading_System
//
//  Created by Huiyou Chen on 12/5/16.
//  Copyright © 2016 Francis Chen. All rights reserved.

//  The implementation of BondHistoricalDataService refers a lot from @Zilun Shen. Credit to him.

#ifndef BondHistoricalDataService_hpp
#define BondHistoricalDataService_hpp
#include "HistoricalDataService.hpp"
#include "BondPositionService.hpp"
#include "BondRiskService.hpp"
#include "BondExecutionService.hpp"
#include "BondInquiryService.hpp"
#include "BondStreamingService.hpp"
#include "DataTranslator.hpp"
#include <memory>
#include <fstream>
#include <type_traits>
#include <typeinfo>

static vector<string> CUSIP_LIST {"912828A75", "912828G95", "912828G87", "912828U57", "912828U24", "912810RU4"};

class BondHistoricalDataService : public HistoricalDataService<Bond>
{
public:
    BondHistoricalDataService();

    virtual ~BondHistoricalDataService() = default;

    HistoricalData<Bond>& GetData(string key) override;

    void OnMessage(HistoricalData<Bond> &data) override;

    void AddListener(ServiceListener<HistoricalData<Bond> > *listener) override;

    const vector<ServiceListener<HistoricalData<Bond> >*>& GetListeners() const override;

    // Persist data to a store
    void PersistData(string persistKey, const HistoricalData<Bond>& data) override;
private:
    map<string, shared_ptr<FileOutput> > all_output;
    map<string, HistoricalData<Bond> > _histData;
    vector<ServiceListener<HistoricalData<Bond> >*> _histDataListeners;

};

template<typename S, typename T>
void Connect(BondHistoricalDataService* hist_service, S *target_service, string service_key, T &data){
    if(is_base_of<Service<string, PV01<Bond>>,S>::value){
        for(auto& key : CUSIP_LIST){
            hist_service->PersistData(service_key, HistoricalData<Bond>(key,dynamic_cast<BondRiskService*>(target_service)->GetRisk(Bond(key))));
        }
        const vector<BucketedSector<Bond>> BUCKETED_SECTOR_LIST{
            BucketedSector<Bond>({Bond(CUSIP_LIST[0]), Bond(CUSIP_LIST[1])}, "Short"),
            BucketedSector<Bond>({Bond(CUSIP_LIST[2]), Bond(CUSIP_LIST[3]), Bond(CUSIP_LIST[4])}, "Middle"),
            BucketedSector<Bond>({Bond(CUSIP_LIST[5])}, "Long")
        };
        for(auto& sector : BUCKETED_SECTOR_LIST){
            hist_service->PersistData(service_key, HistoricalData<Bond>(sector.GetName(),dynamic_cast<BondRiskService*>(target_service)->GetBucketedRisk(sector)));
        }
        hist_service->PersistData(service_key, HistoricalData<Bond>());
    }
    else{
        hist_service->PersistData(service_key, data);
    }
}

template<typename T>
class BondHistoricalDataListener: public ServiceListener<T>
{
public:
    BondHistoricalDataListener(string service, Service<string,T> &target_service, BondHistoricalDataService &hist_service);
    void ProcessAdd(T &data) override;
    void ProcessRemove(T &data) override {};
    void ProcessUpdate(T &data) override {};
private:
    string _serviceKey;
    BondHistoricalDataService *_histService;
    Service<string, T> *_targetService;
};

template<typename T>
BondHistoricalDataListener<T>::BondHistoricalDataListener(string service, Service<string,T> &target_service, BondHistoricalDataService &hist_service): _serviceKey(service), _targetService(&target_service), _histService(&hist_service)
{
    _targetService->AddListener(this);
}

template<typename T>
void BondHistoricalDataListener<T>::ProcessAdd(T &data){
    Connect(_histService, _targetService, _serviceKey, data);
}


const string HISTORYDATA_OUTPUT_POSITION = "./output/position.txt";
const string HISTORYDATA_OUTPUT_RISK = "./output/risk.txt";
const string HISTORYDATA_OUTPUT_EXECUTION = "./output/execution.txt";
const string HISTORYDATA_OUTPUT_STREAMING = "./output/streaming.txt";
const string HISTORYDATA_OUTPUT_INQUIRY = "./output/allinquiries.txt";

const string SERVICE_KEY_POSITION    = "Position Service";
const string SERVICE_KEY_RISK        = "Risk Service";
const string SERVICE_KEY_EXECUTION   = "Execution Service";
const string SERVICE_KEY_STREAMING   = "Streaming Service";
const string SERVICE_KEY_INQUIRY	 = "Inquiry Service";

#endif /* BondHistoricalDataService_h */
