//
//  Products.cpp
//  Bond_Trading_System
//
//  Created by Huiyou Chen on 12/16/16.
//  Copyright © 2016 Francis Chen. All rights reserved.
//

#include "Products.hpp"
Product::Product(string _productId, ProductType _productType)
{
    productId = _productId;
    productType = _productType;
}

const string& Product::GetProductId() const
{
    return productId;
}

ProductType Product::GetProductType() const
{
    return productType;
}

Bond::Bond(string _productId, BondIdType _bondIdType, string _ticker, float _coupon, date _maturityDate) : Product(_productId, BOND)
{
    bondIdType = _bondIdType;
    ticker = _ticker;
    coupon = _coupon;
    maturityDate =_maturityDate;
}

/*
 912828A75, 1.500%, 12/31/2018
 912828G95, 1.625%, 12/31/2019
 912828G87, 2.125%, 12/31/2021
 912828U57, 2.125%, 11/30/2023
 912828U24, 2.000%, 11/15/2026
 912810RU4, 2.875%, 11/15/2046
 */
Bond::Bond(const string& _prodcutId) : Product(0, BOND){
    bondIdType = CUSIP;
    ticker = "T";
    if(_prodcutId=="912828A75") {
        maturityDate = *new date(2018,Dec,31); coupon = 1.5;}
    if(_prodcutId=="912828G95") {
        maturityDate = *new date(2019,Dec,31); coupon = 1.625;}
    if(_prodcutId=="912828G87") {
        maturityDate = *new date(2021,Dec,31); coupon = 2.125;}
    if(_prodcutId=="912828U57") {
        maturityDate = *new date(2023,Nov,30); coupon = 2.125;}
    if(_prodcutId=="912828U24") {
        maturityDate = *new date(2026,Nov,15); coupon = 2.0;}
    if(_prodcutId=="912810RU4") {
        maturityDate = *new date(2046,Nov,15); coupon = 2.875;}
    else {
        maturityDate = *new date(2018,Dec,31); coupon = 2.0;}
}

Bond::Bond() : Product(0, BOND)
{
}

const string& Bond::GetTicker() const
{
    return ticker;
}

float Bond::GetCoupon() const
{
    return coupon;
}

const date& Bond::GetMaturityDate() const
{
    return maturityDate;
}

BondIdType Bond::GetBondIdType() const
{
    return bondIdType;
}
Bond& Bond::operator = (const Bond& bond){
    if(&bond == this) return *this;
    else{
        productId = bond.productId;
        bondIdType = bond.bondIdType;
        ticker = bond.ticker;
        coupon = bond.coupon;
        maturityDate = bond.maturityDate;
        return *this;
    }
}

ostream& operator<<(ostream &output, const Bond &bond)
{
    output << bond.GetBondIdType() << ", " << bond.GetProductId() << ", " << bond.GetTicker() << ", " << bond.GetCoupon() << ", " << bond.GetMaturityDate();
    return output;
}

IRSwap::IRSwap(string _productId, DayCountConvention _fixedLegDayCountConvention, DayCountConvention _floatingLegDayCountConvention, PaymentFrequency _fixedLegPaymentFrequency, FloatingIndex _floatingIndex, FloatingIndexTenor _floatingIndexTenor, date _effectiveDate, date _terminationDate, Currency _currency, int _termYears, SwapType _swapType, SwapLegType _swapLegType) :
Product(_productId, IRSWAP)
{
    fixedLegDayCountConvention =_fixedLegDayCountConvention;
    floatingLegDayCountConvention =_floatingLegDayCountConvention;
    fixedLegPaymentFrequency =_fixedLegPaymentFrequency;
    floatingIndex =_floatingIndex;
    floatingIndexTenor =_floatingIndexTenor;
    effectiveDate =_effectiveDate;
    terminationDate =_terminationDate;
    currency =_currency;
    termYears = _termYears;
    swapType =_swapType;
    swapLegType = _swapLegType;
    effectiveDate =_effectiveDate;
    terminationDate =_terminationDate;
}

IRSwap::IRSwap() : Product(0, IRSWAP)
{
}

DayCountConvention IRSwap::GetFixedLegDayCountConvention() const
{
    return fixedLegDayCountConvention;
}

DayCountConvention IRSwap::GetFloatingLegDayCountConvention() const
{
    return floatingLegDayCountConvention;
}

PaymentFrequency IRSwap::GetFixedLegPaymentFrequency() const
{
    return fixedLegPaymentFrequency;
}

FloatingIndex IRSwap::GetFloatingIndex() const
{
    return floatingIndex;
}

FloatingIndexTenor IRSwap::GetFloatingIndexTenor() const
{
    return floatingIndexTenor;
}

const date& IRSwap::GetEffectiveDate() const
{
    return effectiveDate;
}

const date& IRSwap::GetTerminationDate() const
{
    return terminationDate;
}

Currency IRSwap::GetCurrency() const
{
    return currency;
}

int IRSwap::GetTermYears() const
{
    return termYears;
}

SwapType IRSwap::GetSwapType() const
{
    return swapType;
}

SwapLegType IRSwap::GetSwapLegType() const
{
    return swapLegType;
}


ostream& operator<<(ostream &output, const IRSwap &swap)
{
    output << "fixedDayCount:" << swap.ToString(swap.GetFixedLegDayCountConvention()) << " floatingDayCount:" << swap.ToString(swap.GetFloatingLegDayCountConvention()) << " paymentFreq:" << swap.ToString(swap.GetFixedLegPaymentFrequency()) << " " << swap.ToString(swap.GetFloatingIndexTenor()) << swap.ToString(swap.GetFloatingIndex()) << " effective:" << swap.GetEffectiveDate() << " termination:" << swap.GetTerminationDate() << " " << swap.ToString(swap.GetCurrency()) << " " << swap.GetTermYears() << "yrs " << swap.ToString(swap.GetSwapType()) << " " << swap.ToString(swap.GetSwapLegType());
    return output;
}

string IRSwap::ToString(DayCountConvention dayCountConvention) const
{
    switch (dayCountConvention) {
        case THIRTY_THREE_SIXTY: return "30/360";
        case ACT_THREE_SIXTY: return "Act/360";
        default: return "";
    }
}

string IRSwap::ToString(PaymentFrequency paymentFrequency) const
{
    switch (paymentFrequency) {
        case QUARTERLY: return "Quarterly";
        case SEMI_ANNUAL: return "Semi-Annual";
        case ANNUAL: return "Annual";
        default: return "";
    }
}

string IRSwap::ToString(FloatingIndex floatingIndex) const
{
    switch (floatingIndex) {
        case LIBOR: return "LIBOR";
        case EURIBOR: return "EURIBOR";
        default: return "";
    }
}

string IRSwap::ToString(FloatingIndexTenor floatingIndexTenor) const
{
    switch(floatingIndexTenor) {
        case TENOR_1M: return "1m";
        case TENOR_3M: return "3m";
        case TENOR_6M: return "6m";
        case TENOR_12M: return "12m";
        default: return "";
    }
}

string IRSwap::ToString(Currency currency) const
{
    switch(currency) {
        case USD: return "USD";
        case EUR: return "EUR";
        case GBP: return "GBP";
        default: return "";
    }
}

string IRSwap::ToString(SwapType swapType) const
{
    switch(swapType) {
        case STANDARD: return "Standard";
        case FORWARD: return "Forward";
        case IMM: return "IMM";
        case MAC: return "MAC";
        case BASIS: return "Basis";
        default: return "";
    }
}

string IRSwap::ToString(SwapLegType swapLegType) const
{ 
    switch(swapLegType) {
        case OUTRIGHT: return "Outright";
        case CURVE: return "Curve";
        case FLY: return "Fly";
        default: return "";
    }
}
