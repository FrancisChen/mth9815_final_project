//
//  BondPositionListener.cpp
//  Bond_Trading_System
//
//  Created by Huiyou Chen on 12/18/16.
//  Copyright © 2016 Francis Chen. All rights reserved.
//

#include "BondPositionListener.hpp"
BondPositionListener::BondPositionListener(BondTradeBookingService& tradeService, BondPositionService& positionService){
    tradeService.AddListener(this);
    _positionService = &positionService;
}

void BondPositionListener::ProcessAdd(Trade<Bond> &data){
    _positionService->AddTrade(data);
}

void BondPositionListener::AddReceiver(BondPositionService& receiver){
    _positionService = &receiver;
}
