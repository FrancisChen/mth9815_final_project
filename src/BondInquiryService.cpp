//
//  BondInquiryService.cpp
//  Bond_Trading_System
//
//  Created by Huiyou Chen on 12/18/16.
//  Copyright © 2016 Francis Chen. All rights reserved.
//

#include "BondInquiryService.hpp"
Inquiry<Bond>& BondInquiryService::GetData(string key){
    if(_inquiry.find(key)!=_inquiry.end())
        return _inquiry[key];
    else {
        cout << "No Execution Data!" << endl;
        exit(-1);
    }
}

void BondInquiryService::OnMessage(Inquiry<Bond>& data){
    _inquiry[data.GetProduct().GetProductId()] = data;
    for(auto iter : _inquiryListeners)
        iter->ProcessAdd(data);
}

void BondInquiryService::AddListener(ServiceListener<Inquiry<Bond> > *listener){
    _inquiryListeners.push_back(listener);
}

const vector<ServiceListener<Inquiry<Bond> >*>& BondInquiryService::GetListeners() const{
    return _inquiryListeners;
}

ostream& operator<<(ostream &output, const BondInquiryService &source){
    for(auto iter : source._inquiry){
        output << iter.second.GetInquiryId() << ", " <<
                  iter.second.GetState() << ", " <<
                  iter.second.GetProduct().GetProductId() << ", " <<
                  iter.second.GetPrice() << ", " <<
                  iter.second.GetQuantity() << ", " <<
                  iter.second.GetSide() << "\n";
    }
    return output;
}

void BondInquiryService::SendQuote(const string &inquiryId, double price)
{
    GetData(inquiryId).UpdatePrice(price);
    GetData(inquiryId).UpdateState(InquiryState::QUOTED);
    conn->Publish(GetData(inquiryId));
}

void BondInquiryService::RejectInquiry(const string &inquiryId){
    GetData(inquiryId).UpdateState(InquiryState::REJECTED);
    conn->Publish(GetData(inquiryId));

}
