//
//  BondInquiryService.h
//  Bond_Trading_System
//
//  Created by Huiyou Chen on 12/5/16.
//  Copyright © 2016 Francis Chen. All rights reserved.
//

#ifndef BondInquiryService_hpp
#define BondInquiryService_hpp
#include "InquiryService.hpp"
#include "Products.hpp"

class BondInquiryService: public InquiryService<Bond>
{
public:
    BondInquiryService() = default;

    virtual ~BondInquiryService() = default;

    Inquiry<Bond>& GetData(string key) override;

    void OnMessage(Inquiry<Bond>& data) override;

    void AddListener(ServiceListener<Inquiry<Bond> > *listener) override;

    const vector<ServiceListener<Inquiry<Bond> >*>& GetListeners() const override;

    // Send a quote back to the client
    void SendQuote(const string &inquiryId, double price) override;

    // Reject an inquiry from the client
    void RejectInquiry(const string &inquiryId) override;

    friend ostream& operator<<(ostream &output, const BondInquiryService &source);
private:
    map<string, Inquiry<Bond> > _inquiry;
    Connector<Inquiry<Bond>> *conn;
    vector<ServiceListener<Inquiry<Bond> >*> _inquiryListeners;
};
#endif /* BondInquiryService_h */
