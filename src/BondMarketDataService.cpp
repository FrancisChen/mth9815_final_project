//
//  BondMarketDataService.cpp
//  Bond_Trading_System
//
//  Created by Huiyou Chen on 12/18/16.
//  Copyright © 2016 Francis Chen. All rights reserved.
//

#include "BondMarketDataService.hpp"
// Get the best bid/offer order
const BidOffer BondMarketDataService::GetBestBidOffer(const string &productId){
    double best_bid = 0, best_offer = 1000;
    long bid_quantity = 0, offer_quantity = 0;
    if(_marketData.find(productId)!=_marketData.end()){
        for(auto curr_bid : _marketData.at(productId).GetBidStack()){
            if(curr_bid.GetPrice() > best_bid){
                best_bid = curr_bid.GetPrice();
                bid_quantity = curr_bid.GetQuantity();
            }
        }
        for(auto curr_offer : _marketData.at(productId).GetOfferStack()){
            if(curr_offer.GetPrice() < best_offer){
                best_offer = curr_offer.GetPrice();
                offer_quantity = curr_offer.GetQuantity();
            }
        }
        Order bid_order(best_bid, bid_quantity, BID);
        Order offer_order(best_offer, offer_quantity, OFFER);
        BidOffer bestBidOffer(bid_order, offer_order);
        return bestBidOffer;
    }
    else{
        cout << "No Bid-Offer Data!" << endl;
        exit(-1);
    }
}

// Aggregate the order book
const long BondMarketDataService::AggregateDepth(const string &productId, const double price){
    long vol = 0;
    if(_marketData.find(productId)!=_marketData.end()){
        for(auto bid : _marketData.at(productId).GetBidStack()){
            if(bid.GetPrice() == price)
                vol += bid.GetQuantity();
        }
        for(auto offer : _marketData.at(productId).GetOfferStack()){
            if(offer.GetPrice() == price)
                vol += offer.GetQuantity();
        }
        return vol;
    }
    else exit(-1);
}

OrderBook<Bond>& BondMarketDataService::GetData(string key){
    if(_marketData.find(key)!=_marketData.end())
        return _marketData[key];
    else exit(-1);
}

void BondMarketDataService::OnMessage(OrderBook<Bond>& data){
    _marketData[data.GetProduct().GetProductId()] = data;
    for(auto iter : _marketDataListeners)
        iter->ProcessAdd(data);
}

void BondMarketDataService::AddListener(ServiceListener<OrderBook<Bond> > *listener){
    _marketDataListeners.push_back(listener);
}

const vector<ServiceListener<OrderBook<Bond> >*>& BondMarketDataService::GetListeners() const{
    return _marketDataListeners;
}

//ostream& operator<<(ostream &output, const  BondMarketDataService &source){
//    for(auto iter : source._marketData){
//        output << iter.second.GetProduct().GetProductId() << ", " << iter.second.GetQuantity()<< ", " << iter.second.GetQuantity() << "\n";
//    }
//    return output;
//}
