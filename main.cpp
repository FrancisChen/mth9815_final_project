/**
 * Bond Trading System Final Project
 * main.cpp
 * @author Huiyou (Francis) Chen.
 * created on Dec.5, 2016
 * Copyright � 2016 Francis Chen. All rights reserved.
 */

#include <algorithm>
#include <iterator>
#include <fstream>
#include <iostream>
#include <sstream>
<<<<<<< HEAD
#include "src/InputFileGenerator.hpp"
#include "src/BondTradeBookingService.hpp"
#include "src/BondPositionService.hpp"
#include "src/BondPricingService.hpp"
#include "src/BondRiskService.hpp"
#include "src/BondMarketdataService.hpp"
#include "src/BondAlgoStreamingService.hpp"
#include "src/BondStreamingService.hpp"
#include "src/BondInquiryService.hpp"
#include "src/BondAlgoExecutionService.hpp"
#include "src/BondExecutionService.hpp"
#include "src/BondHistoricalDataService.hpp"
#include "src/BondPositionListener.hpp"
#include "src/BondRiskListener.hpp"
#include "src/BondExecutionListener.hpp"
#include "src/BondStreamingListener.hpp"
#include "src/BondTradeBookingConnector.hpp"
#include "src/BondPricingConnector.hpp"
#include "src/BondInquiryConnector.hpp"
#include "src/BondMarketDataConnector.hpp"
=======
#include "srt/InputFileGenerator.hpp"
#include "srt/BondTradeBookingService.hpp"
#include "srt/BondPositionService.hpp"
#include "srt/BondPricingService.hpp"
#include "srt/BondRiskService.hpp"
#include "srt/BondMarketdataService.hpp"
#include "srt/BondAlgoStreamingService.hpp"
#include "srt/BondStreamingService.hpp"
#include "srt/BondInquiryService.hpp"
#include "srt/BondAlgoExecutionService.hpp"
#include "srt/BondExecutionService.hpp"
#include "srt/BondHistoricalDataService.hpp"
#include "srt/BondPositionListener.hpp"
#include "srt/BondRiskListener.hpp"
#include "srt/BondExecutionListener.hpp"
#include "srt/BondStreamingListener.hpp"
#include "srt/BondTradeBookingConnector.hpp"
#include "srt/BondPricingConnector.hpp"
#include "srt/BondInquiryConnector.hpp"
#include "srt/BondMarketDataConnector.hpp"
>>>>>>> 2fc9d0a2810715ba33c0d3eae6f566a3ef740c22

using namespace std;

// input file names
string TRADE_INPUT_FILE = "./input/trades.txt";

string PRICE_INPUT_FILE = "./input/price.txt";

string MARKETDATA_INPUT_FILE = "./input/marketdata.txt";

string INQUIRY_INPUT_FILE = "./input/inquiry.txt";

string BONDSTREAM_OUTPUT_FILE = "./output/bondstream.txt";

void testTradeService(){
    cout << "****** Generating Trade Data ******" << endl;
    GetTradeFile(TRADE_INPUT_FILE);
    cout << "****** Successful! ******" << endl;
    BondPositionService position_service;
    BondTradeBookingService trade_service;
    BondPositionListener position_listener(trade_service, position_service);

    BondRiskService risk_service;
    BondRiskListener risk_listener(position_service, risk_service);

    BondHistoricalDataService hist_service;
    BondHistoricalDataListener<Position<Bond>> hist_position_listener(SERVICE_KEY_POSITION, position_service, hist_service);
    BondHistoricalDataListener<PV01<Bond>> hist_risk_listener(SERVICE_KEY_RISK, risk_service, hist_service);

    cout<< "****** Sending Data ******" << endl;
    BondTradeBookingConnector conn(trade_service); conn.FileReader(TRADE_INPUT_FILE);
    cout << "****** Done! ******" <<endl;

}
void testPricingService(){
    cout << "****** Generating Price Data ******" <<  endl;
    GetPriceFile(PRICE_INPUT_FILE);
    cout << "****** Successful! ******" << endl;

    BondPricingService prc_service;
    // Output price stream
    BondAlgoStreamingService algostream_service;
    BondStreamingService stream_service(BONDSTREAM_OUTPUT_FILE);
    BondAlgoStreamingListener algostream_listener(prc_service, algostream_service);
    BondStreamingListener stream_listener(algostream_service, stream_service);

    BondHistoricalDataService hist_service;
    BondHistoricalDataListener<PriceStream<Bond>> hist_stream_listener(SERVICE_KEY_STREAMING, stream_service, hist_service);

    cout << "****** Sending Price Data ******" <<  endl;
    BondPricingConnector prc_conn(prc_service); prc_conn.FileReader(PRICE_INPUT_FILE);

    cout << "****** Done! ******" <<endl;

}
void testMarketDataService(){
    BondMarketDataService mkt_service;
    BondMarketDataConnector mkt_conn(mkt_service);

//    auto make_order_stack = [=](vector<double> price, PricingSide side){
//        vector<Order> stk;
//        for(auto i : price){
//            stk.push_back(Order(i, 1000, side));
//        }
//        return stk;
//    };

    //mkt_conn.AddOrderBook(OrderBook<Bond>(Bond(CUSIPS_LIST[0]), make_order_stack({99.998, 99.997, 99.994, 99.998}, BID), make_order_stack({100.001, 100.002, 100.001, 100.001}, OFFER)));
    
    GetMarketDataFile(MARKETDATA_INPUT_FILE);
    cout << "****** Generating Market Data ******" << endl;
    mkt_conn.FileReader(MARKETDATA_INPUT_FILE);
    cout << "****** Successful! ******" << endl;
 
    BondAlgoExecutionService algo_service;
    BondAlgoExcutionListener algo_listener(mkt_service, algo_service);
    BondExecutionService execution_service;
    BondExecutionListener execution_listener(algo_service, execution_service);

    BondHistoricalDataService hist_service;
    BondHistoricalDataListener<ExecutionOrder<Bond>> hist_execution_listener(SERVICE_KEY_EXECUTION, execution_service, hist_service);
    
    cout << "****** Sending Market Data ******" <<  endl;
    cout << "****** Done! ******" <<endl;
}
void testInquiryService(){
    cout << "****** Generating Inquiry File ******" << endl;
    GetInquiryFile(INQUIRY_INPUT_FILE);
    cout << "****** Successful! ******" << endl;
    BondInquiryService inq_service;
    BondInquiryConnector inq_conn(inq_service);

    BondHistoricalDataService hist_service;
    BondHistoricalDataListener<Inquiry<Bond>> hist_inq_listener(SERVICE_KEY_INQUIRY, inq_service, hist_service);

    inq_conn.FileReader(INQUIRY_INPUT_FILE);
    cout << "****** Done! ******" << endl;
}

int main() {
    // insert code here...
    cout << "Baruch MFE: MTH9815 Software Engineering for Finance\n";
    cout << "Bond Trading System Final Project\n";
    testTradeService();
    testPricingService();
    testInquiryService();
    testMarketDataService();

    return 0;
}
